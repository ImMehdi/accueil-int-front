'use strict'
let express = require('express')
let fs = require('fs')
let app = express()
let info = []
let jsonArray = []
let methodOverride = require('method-override')

const util = require('util');
const readFile = util.promisify(fs.readFile)
const writeFilePromise = util.promisify(fs.writeFile)

app.use(express.bodyParser())
app.use(methodOverride('_method'))
app.set('views', '/home/elmehdi/Desktop/Studies/JavaScript/JSAU/jsau-webserver/src/views')
app.set('view engine', 'ejs')


app.post('/', (request, response) => {
    if (request.body.firstName != '' && request.body.familyName != '' && request.body.email != '') {
        info.push(request.body.firstName, request.body.familyName, request.body.email)
        jsonArray.push(JSON.stringify({FirstName:info[0], FamilyName:info[1], Email:info[2]}))
        info = []
        async function readF(){
          return await readFile('src/Model/informationFile.json')
        }
        async function writeF(){
          return await writeFilePromise('src/Model/informationFile.json','[' + jsonArray + ']')
        }

        readF().then(result => console.log(result))
        .catch(error => console.log(error))

        writeF().then(result => console.log(result))
        .catch(error => console.log(error))

        response.render('FormulaireSuccess')
    } else {
        response.render('FormulaireWarining')
    }
})


app.post('/AllInfo', (request, response) => {

    response.render('AllInfo', {info : jsonArray})

})

app.post('/Modify', (request, response) => {
  let row_To_Modify = request.body.row - 1
    jsonArray.splice(row_To_Modify, 1)
    info.push(request.body.firstName, request.body.familyName, request.body.email)
    jsonArray.push(JSON.stringify({FirstName:info[0], FamilyName:info[1], Email:info[2]}))
    info = []
    fs.readFile('src/Model/informationFile.json', (err, data) => {
        if (err) {
            throw err
        }
    })

    fs.writeFile('src/Model/informationFile.json', '[' + jsonArray + ']', (err, result) => {

    })
    response.render('AllInfo', {info : jsonArray})
})
app.get('/', (request, response) => {

    response.render('Formulaire')

})

app.post('/Return', (request, response) => {
    response.render('Formulaire')
})

app.post('/Deleteit', (request, response) => {
    let row_To_Delete = request.body.row - 1
    jsonArray.splice(row_To_Delete, 1)
    response.render('AllInfo', {info : jsonArray})
})

app.listen(8050)
