import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()

export class ShareServiceService {

  private prenomSource = new BehaviorSubject("");
  private nomSource = new BehaviorSubject("");
  private emailSource = new BehaviorSubject("");
  private adresseSource = new BehaviorSubject("");
  private citySource = new BehaviorSubject("");
  private zipSource = new BehaviorSubject("");
  private casSource = new BehaviorSubject("");
  private nationalitySource = new BehaviorSubject("");
  private statutSource = new BehaviorSubject("");

  bool: boolean = false;

  currentPrenom = this.prenomSource.asObservable();
  currentNom = this.nomSource.asObservable();
  currentEmail = this.emailSource.asObservable();
  currentAdresse = this.adresseSource.asObservable();
  currentCity = this.citySource.asObservable();
  currentZip = this.zipSource.asObservable();
  currentCas = this.casSource.asObservable();
  currentStatut = this.statutSource.asObservable();
  currentNationality = this.nationalitySource.asObservable();

  constructor() { }

  changePrenom(prenom: string) {
    this.prenomSource.next(prenom)
  }

  changeNom(nom: string) {
    this.nomSource.next(nom)
  }

  changeEmail(email: string) {
    this.emailSource.next(email)
  }

  changeCas(cas: string){
      this.casSource.next(cas)
  }

  changeStatut(statut: string){
    this.statutSource.next(statut)
  }

  changeNationality(nationality: string){
    this.nationalitySource.next(nationality)
  }

  changeAdresse(adresse: string) {
    this.adresseSource.next(adresse)
  }

  changeCity(city: string) {
    this.citySource.next(city)
  }

  changeZip(zip: string) {
    this.zipSource.next(zip)
  }

}
