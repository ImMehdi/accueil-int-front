import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoadingSpinnerComponent } from './ui/loading-spinner/loading-spinner.component';
import { TopMenuComponent } from './ui/top-menu/top-menu.component';
import { ImagesComponent } from './ui/images/images.component';
import { FormComponent } from './ui/form/form.component';
import { FormsModule } from '@angular/forms';
import { FeuilleDeRoutePDFComponent } from './ui/feuille-de-route-pdf/feuille-de-route-pdf.component';
import { ShareServiceService } from './share-service.service'
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { FooterComponent } from './ui/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    LoadingSpinnerComponent,
    TopMenuComponent,
    ImagesComponent,
    FormComponent,
    FeuilleDeRoutePDFComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    PdfViewerModule
  ],
  providers: [ShareServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
