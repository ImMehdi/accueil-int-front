import { Component, OnInit } from '@angular/core';
import { ShareServiceService } from "../../share-service.service";
import { FeuilleDeRoutePDFComponent } from '../feuille-de-route-pdf/feuille-de-route-pdf.component';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  firstName: string;
  lastName: string;
  email: string;
  cas: string;
  statut: string;
  nationality: string;
  adress: string;
  city: string;
  zip: string;

  constructor(private data: ShareServiceService) { }

  ngOnInit() {
    this.data.currentPrenom.subscribe(firstName => this.firstName = firstName)
    this.data.currentNom.subscribe(lastName => this.lastName = lastName)
    this.data.currentEmail.subscribe(email => this.email = email)
    this.data.currentAdresse.subscribe(adress => this.adress = adress)
    this.data.currentCity.subscribe(city => this.city = city)
    this.data.currentZip.subscribe(zip => this.zip = zip)
    this.data.currentCas.subscribe(cas => this.cas = cas)
    this.data.currentStatut.subscribe(statut => this.statut = statut)
    this.data.currentNationality.subscribe(nationality => this.nationality = nationality)


  }

  onSubmit(){
    this.data.changePrenom(this.firstName)
    this.data.changeNom(this.lastName)
    this.data.changeEmail(this.email)
    this.data.changeAdresse(this.adress)
    this.data.changeCity(this.city)
    this.data.changeZip(this.zip)
    this.data.changeCas(this.cas)
    this.data.changeStatut(this.statut)
    this.data.changeNationality(this.nationality)
    this.data.bool= true;

  }
}
