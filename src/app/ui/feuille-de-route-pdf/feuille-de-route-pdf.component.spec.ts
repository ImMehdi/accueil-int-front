import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeuilleDeRoutePDFComponent } from './feuille-de-route-pdf.component';

describe('FeuilleDeRoutePDFComponent', () => {
  let component: FeuilleDeRoutePDFComponent;
  let fixture: ComponentFixture<FeuilleDeRoutePDFComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeuilleDeRoutePDFComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeuilleDeRoutePDFComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
