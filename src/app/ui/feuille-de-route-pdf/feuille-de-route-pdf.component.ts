import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ShareServiceService } from "../../share-service.service";
import * as jsPDF from 'jspdf';

@Component({
  selector: 'app-feuille-de-route-pdf',
  templateUrl: './feuille-de-route-pdf.component.html',
  styleUrls: ['./feuille-de-route-pdf.component.css']
})
export class FeuilleDeRoutePDFComponent implements OnInit {

  firstName: string;
  lastName: string;
  email: string;
  cas: string;
  statut: string;
  nationality: string;
  adress: string;
  city: string;
  zip: string;
  pdfSrc: string = "assets/pdf/rest.pdf";
  show: boolean = false;

  constructor(private data: ShareServiceService) { }

  ngOnInit() {
    this.data.currentPrenom.subscribe(firstName => this.firstName = firstName)
    this.data.currentNom.subscribe(lastName => this.lastName = lastName)
    this.data.currentEmail.subscribe(email => this.email = email)
    this.data.currentAdresse.subscribe(adress => this.adress = adress)
    this.data.currentCity.subscribe(city => this.city = city)
    this.data.currentZip.subscribe(zip => this.zip = zip)
    this.data.currentCas.subscribe(cas => this.cas = cas)
    this.data.currentStatut.subscribe(statut => this.statut = statut)
    this.data.currentNationality.subscribe(nationality => this.nationality = nationality)
    this.show=this.data.bool;
  }

 @ViewChild('content') content :ElementRef;
 public DownloadPDF(){
   let doc = new jsPDF();
   let specialElementHandlers = {
     '#editor': function(element, renderer){
       return true;
     }
   };
   let content = this.content.nativeElement;
   doc.fromHTML(content.innerHTML, 15, 15, {
     'width': 190,
     'elementHandlers': specialElementHandlers
   });
   console.log("Here");
   doc.save('FeuilleDeRoute.pdf');

 }

}
