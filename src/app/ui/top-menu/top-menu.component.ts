import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.css']
})
export class TopMenuComponent implements OnInit {

  private timer;

  constructor() { }

  ngOnInit() {

    this.timer = setTimeout(() =>   $("div").fadeIn("slow"), 1500);

  }

}
