import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading-spinner',
  templateUrl: './loading-spinner.component.html',
  styleUrls: ['./loading-spinner.component.scss']
})
export class LoadingSpinnerComponent implements OnInit {

  showSpinner: boolean = true;
  private timer;

  constructor() {
  }

  ngOnInit() {
    this.timer = setTimeout(() => this.showSpinner= false, 1500);
  }

}
